﻿namespace KeywordsFromUrl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxUrlAddress = new System.Windows.Forms.TextBox();
            this.labelUrlAddress = new System.Windows.Forms.Label();
            this.ButtonCountKeywords = new System.Windows.Forms.Button();
            this.listViewKeywordsStats = new System.Windows.Forms.ListView();
            this.Keywords = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Occurrence = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // textBoxUrlAddress
            // 
            this.textBoxUrlAddress.Location = new System.Drawing.Point(30, 28);
            this.textBoxUrlAddress.Name = "textBoxUrlAddress";
            this.textBoxUrlAddress.Size = new System.Drawing.Size(213, 20);
            this.textBoxUrlAddress.TabIndex = 0;
            // 
            // labelUrlAddress
            // 
            this.labelUrlAddress.AutoSize = true;
            this.labelUrlAddress.Location = new System.Drawing.Point(30, 9);
            this.labelUrlAddress.Name = "labelUrlAddress";
            this.labelUrlAddress.Size = new System.Drawing.Size(69, 13);
            this.labelUrlAddress.TabIndex = 1;
            this.labelUrlAddress.Text = "URL address";
            // 
            // ButtonCountKeywords
            // 
            this.ButtonCountKeywords.Location = new System.Drawing.Point(33, 418);
            this.ButtonCountKeywords.Name = "ButtonCountKeywords";
            this.ButtonCountKeywords.Size = new System.Drawing.Size(115, 23);
            this.ButtonCountKeywords.TabIndex = 4;
            this.ButtonCountKeywords.Text = "Count Keywords";
            this.ButtonCountKeywords.UseVisualStyleBackColor = true;
            this.ButtonCountKeywords.Click += new System.EventHandler(this.ButtonCountKeywords_Click);
            // 
            // listViewKeywordsStats
            // 
            this.listViewKeywordsStats.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Keywords,
            this.Occurrence});
            this.listViewKeywordsStats.Location = new System.Drawing.Point(27, 87);
            this.listViewKeywordsStats.Name = "listViewKeywordsStats";
            this.listViewKeywordsStats.Size = new System.Drawing.Size(273, 325);
            this.listViewKeywordsStats.TabIndex = 5;
            this.listViewKeywordsStats.UseCompatibleStateImageBehavior = false;
            this.listViewKeywordsStats.View = System.Windows.Forms.View.Details;
            // 
            // Keywords
            // 
            this.Keywords.DisplayIndex = 1;
            this.Keywords.Text = "Keywords";
            this.Keywords.Width = 144;
            // 
            // Occurrence
            // 
            this.Occurrence.DisplayIndex = 0;
            this.Occurrence.Text = "Occurrences";
            this.Occurrence.Width = 128;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 460);
            this.Controls.Add(this.listViewKeywordsStats);
            this.Controls.Add(this.ButtonCountKeywords);
            this.Controls.Add(this.labelUrlAddress);
            this.Controls.Add(this.textBoxUrlAddress);
            this.Name = "Form1";
            this.Text = "KeywordsFromUri";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUrlAddress;
        private System.Windows.Forms.Label labelUrlAddress;
        private System.Windows.Forms.Button ButtonCountKeywords;
        private System.Windows.Forms.ListView listViewKeywordsStats;
        private System.Windows.Forms.ColumnHeader Keywords;
        private System.Windows.Forms.ColumnHeader Occurrence;
    }
}

