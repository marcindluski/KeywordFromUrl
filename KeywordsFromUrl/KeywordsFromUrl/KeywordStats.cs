﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeywordsFromUrl
{  
    /// <summary>
    /// Class stores information about count of occurences of keyword on web page.
    /// </summary>
    public class KeywordStats
    {
        public string Keyword { get; private set; }
        public int NrOfAppearancesOnPage { get; private set; }

        public KeywordStats(string Keyword, int NrOfAppearancesOnPage)
        {
            this.Keyword = Keyword;
            this.NrOfAppearancesOnPage = NrOfAppearancesOnPage;
        }



    }
}
