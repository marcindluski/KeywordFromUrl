﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;


namespace KeywordsFromUrl
{
    /// <summary>
    /// Class finds keywords in html document and counts its occurrences in page text 
    /// and return result as list of KeywordStats objects.
    /// </summary>
    public class StatsCreator
    {
        public List<KeywordStats> KeywordStatsList;
        private string urlAddress;
        private HtmlDocument htmlDocument;
        private string[] keywordsTable;
        public string ErrorLog { get; private set; } //Keeps message about errors.

        /// <summary>
        /// Constructor. In keywordStatsList user will get output data. 
        /// </summary>
        /// <param name="urlAddress">Url address in format. http(s)://(www).webpage.extension/.../...
        /// () - opitional
        /// </param> 
        /// <param name="keywordStatsList">List of KeywordStats object.Output Data.</param> 
        public StatsCreator(string urlAddress, List<KeywordStats> keywordStatsList)
        {
            this.urlAddress = urlAddress;
            htmlDocument = new HtmlDocument();
            this.KeywordStatsList = keywordStatsList;
        }
        /// <summary>
        /// The main function of StatsCreator. Create List of keywordsStas objets.
        /// </summary>
        /// <returns>bool</returns>
        public bool GetStats()
        {
            if (IsUriAddressCorrect())
            {
                if (DownloadHtml())
                {
                    if (CreateKeywordStatsTable())
                    {
                        CountKeyWords();
                        return true;
                    }
                }

            }
           
            return false;
        }
 
        private bool DownloadHtml()
        {
            try
            {
                HttpWebRequest httpRequest = (HttpWebRequest) WebRequest.Create(urlAddress);
                httpRequest.Timeout = 1000; //Wait for response 1000 ms, default: 10000ms
                using (HttpWebResponse httpResponse = (HttpWebResponse) httpRequest.GetResponse())
                {
                    using (StreamReader responseStream = new StreamReader(httpResponse.GetResponseStream()))

                    {
                        String htmlString = responseStream.ReadToEnd();
                        htmlDocument.LoadHtml(htmlString);
                        responseStream.Close();
                    }
                }
                return true;
            }
            catch (WebException)
            {
                ErrorLog = "Incorrect address or server is not responding.";
                return false;
            }
              
            
        }

        private bool IsUriAddressCorrect()
        {
            //Uri pattern.
            Regex uriPattern = new Regex(@"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$");
           
            if (uriPattern.IsMatch(urlAddress))
                return true;

            ErrorLog = "Inccorect uri address!";
            return false;
        }
    


        private bool CreateKeywordStatsTable()
        {
            //Selects all the meta elements that have an attribute named 'name' with a value 'keywords'
            HtmlNode keywordsNode = htmlDocument.DocumentNode.SelectSingleNode("//meta[@name='keywords']");
            if (keywordsNode != null)
            {
                HtmlAttribute contentAttribute = keywordsNode.Attributes["content"];
                string fulldescription = contentAttribute.Value;
                keywordsTable = fulldescription.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);

                // Remove space value from Array. Protection in case that after comma will be epmty value.
                keywordsTable = keywordsTable.Where(val => val != " ").ToArray();
                return true;
            }

            ErrorLog = "Cannot find keyword meta[@name='keywords'] node!";
            return false;

        }

        private void CountKeyWords()
        {
            HtmlNode bodyNode = htmlDocument.DocumentNode.SelectSingleNode("//body");

            // Removes from body nodes that are invisible if you watch at web page from browser.  
            IEnumerable<HtmlNode> nodes = bodyNode.Descendants().Where(n =>  n.NodeType == HtmlNodeType.Text &&
                                                                             n.ParentNode.Name != "script" &&
                                                                             n.ParentNode.Name != "style");
            if (nodes != null)
            {
                // Stores text from body that is visible on web page.
                String bodyNodeContent = null;

                foreach (HtmlNode node in nodes)
                 bodyNodeContent += node.InnerText;
                
                foreach (string keyword in keywordsTable)
                {
                    KeywordStats stats = new KeywordStats(keyword, CountStringOccurrences(bodyNodeContent, keyword));
                    KeywordStatsList.Add(stats);
                }
            }

        }

       

        public int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'. 
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf( pattern.Trim(), i, StringComparison.CurrentCultureIgnoreCase)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }
       
      

    }
}
