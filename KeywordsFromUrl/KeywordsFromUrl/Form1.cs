﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KeywordsFromUrl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonCountKeywords_Click(object sender, EventArgs e)
        {
            listViewKeywordsStats.Clear();
            listViewKeywordsStats.Columns.Add("Keywords",128);
            listViewKeywordsStats.Columns.Add("Occurrences",144);

            string urlAdress = textBoxUrlAddress.Text;
            List<KeywordStats> keywordStatsAndNrOfOccurencesList = new List<KeywordStats>();
            StatsCreator stasCreator = new StatsCreator(urlAdress, keywordStatsAndNrOfOccurencesList);
            

            if (stasCreator.GetStats())
            {
                ListViewItem keywordStatsItem;
                foreach (KeywordStats keywordStats in keywordStatsAndNrOfOccurencesList)
                {
                    keywordStatsItem =
                        new ListViewItem(new[] {keywordStats.Keyword, keywordStats.NrOfAppearancesOnPage.ToString()});
                    listViewKeywordsStats.Items.Add(keywordStatsItem);
                }
            }
            else
            {
                MessageBox.Show(stasCreator.ErrorLog);
            }



        }

    }
}
